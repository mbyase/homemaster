package client

import (
	"io/ioutil"
	"encoding/json"
	"adai.design/homemaster/log"
	"adai.design/homemaster/container"
	"runtime"
)

const (
	uuid = "8bd53509433b457b"
	privateKey = "i9U1CUM7RXuxUVHvSvHP1Q=="
)

// 设备注册信息
type registrationInfo struct {
	UUID      	string	`json:"uuid"`
	HomeId    	string	`json:"home_id"`
	PrivateKey 	string	`json:"private_key"`
}

var registration = func() *registrationInfo {
	var reg registrationInfo
	ok := false
	if runtime.GOARCH == "mipsle" {
		buf, err := ioutil.ReadFile("/etc/homemaster/register")
		if err == nil {
			err = json.Unmarshal(buf, &reg)
			if err == nil {
				ok = true
			}
		}
	}else {
		buf, err := ioutil.ReadFile("build/files/etc/homemaster/register")
		if err == nil {
			err = json.Unmarshal(buf, &reg)
			if err == nil {
				ok = true
			}
		}
	}
	if !ok {
		reg.UUID = uuid
		reg.PrivateKey = privateKey
	}

	log.Info("uuid(%s) private_key(%s)", reg.UUID, reg.PrivateKey)
	container.GetContainer().Id = reg.UUID
	return &reg
}()