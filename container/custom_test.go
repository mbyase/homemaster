package container

import (
	"testing"
	"encoding/json"
)

func TestHomeMasterV2(t *testing.T) {
	buf, err := json.Marshal(GetContainer())
	if err != nil {
		t.Fatal(err)
	}

	t.Log("json: ", string(buf))
}