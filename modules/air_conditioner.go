package modules

import (
	"encoding/binary"
)

// 海尔空调
type haierAirConditioner struct {
	// 海尔空调基本属性
	OnOff 			bool	// 空调开关
	Temperature 	int		// 16-30度
	Mode 			int 	// 0:Cool 1:Heat 2:Dehumidification
	RotationMode 	int 	// 0:向下 1:中 2:向上 3:上下扫风
	WindSpeed 		int 	// 1-4档
	onScreen 		bool	// 屏幕显示

	// 需要发送的字符串
	buffer [14]byte
}


// 海尔空调遥控器编码
// byte[0][0] = 0xA6
// byte[1][0:1] = 健康气流
// byte[1][2:3] = 风向
// byte[1][4:7] = 温度(0-14)->(16-30)
// byte[2]
// byte[3][7] 健康气流
// byte[4][6] OnOff
// byte[5][5:7] 风速	  0x001(1) 0x010(2) 0x011(3) 0x101(自动)
// byte[6]
// byte[7][5] 制冷
// byte[7][6] 除湿
// byte[7][7] 制热
// byte[8]
// byte[9]
// byte[10]
// byte[11]
// byte[12]
// byte[13] = check(sum(byte[0:13])) 校验位

const (
	haierModeCool             = 0
	haierModeHeat             = 1
	haierModeDehumidification = 2
)

// 风速 一档 二挡 三挡 自动风
const (
	haierWindSpeedLevel1 = 0x03
	haierWindSpeedLevel2 = 0x02
	haierWindSpeedLevel3 = 0x01
	haierWindSpeedAuto = 0x05
)

// 风向 上下摆风 上 中 下
const (
	haierWindDirectSwing = 1
	haierWindDirectLower = 2
	haierWindDirectMiddle = 3
	haierWindDirectAbove = 4
)

func (haier *haierAirConditioner) encode() ([]byte, error) {
	for i:=1; i<len(haier.buffer); i++ {
		haier.buffer[i] = 0
	}

	haier.buffer[0] = 0xA6
	haier.buffer[12] = 0x85
	haier.buffer[5] = uint8(haier.WindSpeed) << 5
	haier.buffer[1] = 0x02

	if haier.OnOff {
		haier.buffer[4] |= 0x01 << 6
	}

	if haier.RotationMode == haierWindDirectSwing {
		haier.buffer[1] |= 0x0c
	} else if haier.RotationMode == haierWindDirectLower {
		haier.buffer[1] |= 0x03
	} else if haier.RotationMode == haierWindDirectMiddle {
		haier.buffer[1] |= 0x02
	} else if haier.RotationMode == haierWindDirectAbove {
		haier.buffer[1] |= 0x01
	}

	haier.buffer[1] |= (uint8(haier.Temperature - 16) & 0x0F) << 4
	if haier.Mode == 0x00 {
		haier.buffer[7] = 0x01 << 5
	} else if haier.Mode == 0x01 {
		haier.buffer[7] = 0x01 << 6
	} else if haier.Mode == 0x02 {
		haier.buffer[7] = 0x01 << 7
	}

	// 校验
	var check byte = 0x00
	for i:=0; i<len(haier.buffer) - 1; i++ {
		check += haier.buffer[i]
	}
	haier.buffer[len(haier.buffer) - 1] = check

	// 0101编码
	var data [len(haier.buffer) * 8]byte
	for i:=0; i<len(haier.buffer); i++ {
		for j:=0; j<8; j++ {
			if haier.buffer[i] & (0x01 << uint8(7 - j)) != 0x00 {
				data[i * 8 + j] = 1
			}
		}
	}

	// 波形编码
	var pwm [len(data) * 2 + 4 + 1]int16
	// 引导字段和结束字段
	copy(pwm[0:], []int16{3103,-3043,3092,-4421,})
	pwm[len(pwm) - 1] = 622
	// 数据字段
	for i:=0; i<len(data); i++ {
		pwm[i * 2 + 4] = 622
		if data[i] == 0x01 {
			pwm[2 * i + 1 + 4] = -1606
		} else {
			pwm[2 * i + 1 + 4] = -500
		}
	}


	// 发送字节
	buffer := make([]byte, 2 * len(pwm))
	for i:=0; i<len(pwm); i++ {
		binary.BigEndian.PutUint16(buffer[2*i:], uint16(float32(pwm[i])/26.2*2))
	}

	return buffer, nil
}




