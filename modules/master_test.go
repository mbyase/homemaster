package modules

import (
	"net"
	"testing"
)

func sendTestMessage(msg *MasterMessage) error {
	conn, err := net.Dial("udp", "widora.local:6666")
	if err != nil {
		return err
	}
	defer conn.Close()

	data, err := messageMarshal(msg)
	if err != nil {
		return err
	}

	_, err = conn.Write(data)
	return err
}

func TestMessageDecode(t *testing.T) {
	t.Log("let's go!")


	var end chan bool
	t.Log("chan end: ", end)

	end = make(chan bool, 1)
	end <- true

	t.Log("chan end: ", <-end)

	close(end)

	t.Log("chan end: ", end)
	end <- true

}