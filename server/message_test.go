package server

import (
	"adai.design/homemaster/container/characteristic"
	"adai.design/homemaster/container/service"
	"adai.design/homemaster/log"
	"encoding/json"
	"github.com/gorilla/websocket"
	"testing"
	"time"
)

const TestRemoteIp = "192.168.124.16"

func sendMessage(msg *Message) ([]byte, error) {
	c, _, err := websocket.DefaultDialer.Dial("ws://"+TestRemoteIp+"/homemaster", nil)
	if err != nil {
		return nil, err
	}
	defer c.Close()
	data, _ := json.Marshal(msg)
	log.Info("send: " + string(data))
	err = c.WriteMessage(websocket.TextMessage, data)
	if err != nil {
		return nil, err
	}
	c.SetReadDeadline(time.Now().Add(time.Second * 3))
	_, recv, err := c.ReadMessage()
	if err != nil {
		return nil, err
	}
	c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	return recv, nil
}

// 配件信息获取
func TestAccessoryGet(t *testing.T) {
	msg := &Message{
		Path:   MsgPathContainer,
		Method: MsgMethodGet,
	}

	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

// 状态获取
func TestCharacteristicGet(t *testing.T) {
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodGet,
	}

	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

// 协调器入网许可
func TestCoordinatorPermitJoin(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "5c1f8f83289cb477e1ddb053",
		ServiceId:        20,
		CharacteristicId: 6,
		Value:            200,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

// 关闭开关
func TestSwitchOnOff(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "00158d000288fb3d",
		ServiceId:        1,
		CharacteristicId: 1,
		Value:            0,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

// ------------------------------------------------------------------------------------------------------

// d0cf5efffe11dd8e
//d0cf5efffe0b00d0
//d0cf5efffe11f992

func TestHaierIKEALightOn(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "d0cf5efffe11dd8e",
		ServiceId:        1,
		CharacteristicId: 1,
		Value:            1,
	}, {
		AccessoryID:      "d0cf5efffe0b00d0",
		ServiceId:        1,
		CharacteristicId: 1,
		Value:            1,
	}, {
		AccessoryID:      "d0cf5efffe11f992",
		ServiceId:        1,
		CharacteristicId: 1,
		Value:            1,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestHaierIKEALightOff(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "d0cf5efffe11dd8e",
		ServiceId:        1,
		CharacteristicId: 1,
		Value:            0,
	}, {
		AccessoryID:      "d0cf5efffe0b00d0",
		ServiceId:        1,
		CharacteristicId: 1,
		Value:            0,
	}, {
		AccessoryID:      "d0cf5efffe11f992",
		ServiceId:        1,
		CharacteristicId: 1,
		Value:            0,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestIKEALightLevel(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "d0cf5efffe11f992",
		ServiceId:        1,
		CharacteristicId: 1,
		Value:            0,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestPhilipsHueLevel(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "00178801024490f7",
		ServiceId:        1,
		CharacteristicId: 2,
		Value:            50,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestPhilipsHueOnOff(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "001788010116b471",
		ServiceId:        11,
		CharacteristicId: 4,
		Value:            3000,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestPhilipsHuePlugColor(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "0017880103a0f87e",
		ServiceId:        1,
		CharacteristicId: 3,
		Value:            "0000ff00",
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestIKEAColorTemperature(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "000d6ffffe7c16a2",
		ServiceId:        1,
		CharacteristicId: 2,
		Value:            50,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestPhilipsHueLightOnOff(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "001788010116b471",
		ServiceId:        0x0b,
		CharacteristicId: 1,
		Value:            1,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestPhilipsHueColor(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "001788010116b471",
		ServiceId:        0x0b,
		CharacteristicId: 3,
		Value:            "ff00ff00",
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestHaierAirConditionerOpen(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "f241cd80e3434072",
		ServiceId:        100,
		CharacteristicId: 1,
		Value:            0,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestHaierAirConditionerClose(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "8bd53509433b457b",
		ServiceId:        100,
		CharacteristicId: 1,
		Value:            0,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestHaierAirConditionerSetTemperature(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "8bd53509433b457b",
		ServiceId:        100,
		CharacteristicId: 3,
		Value:            25,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestHaierAirConditionerSetMode(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "8bd53509433b457b",
		ServiceId:        100,
		CharacteristicId: 2,
		Value:            characteristic.AirConditionerModeCool,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

//5c1f8f83289cb477e1ddb053

func TestHaierAirConditionerSetRotation(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "8bd53509433b457b",
		ServiceId:        100,
		CharacteristicId: service.AirConditionerRotationModeCharId,
		Value:            characteristic.AirConditionerWindDirectAbove,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestHaierAirConditionerSetWindSpeed(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID:      "8bd53509433b457b",
		ServiceId:        100,
		CharacteristicId: service.AirConditionerWindSpeedCharId,
		Value:            100,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path:   MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}

func TestWifiSet(t *testing.T) {
	config := map[string]interface{}{
		"ssid":   "#daodao",
		"passwd": "howoldareyou",
	}

	data, _ := json.Marshal(config)
	msg := &Message{
		Path:   MsgPathWifiConfig,
		Method: MsgMethodPost,
		Data:   data,
	}
	recv, err := sendMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("recv: ", string(recv))
}
