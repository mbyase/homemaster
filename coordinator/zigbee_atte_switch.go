package coordinator

import (
	"adai.design/homemaster/container/accessory"
	"adai.design/homemaster/container/characteristic"
	"adai.design/homemaster/log"
	"encoding/hex"
	"github.com/gorilla/websocket"
)

type zigbeeSwitch struct {
	*ZigbeeDescriptor
	acc *accessory.SwitchChan3
}

func (z *zigbeeSwitch) update(data []byte) error {
	//srcAddr := binary.BigEndian.Uint16(data[1:])
	endpoint := data[3]
	var value byte
	if len(data) < 9 {
		value = data[7]
	} else {
		value = data[11]
	}

	//log.Printf("attribute: netaddr(%04x) endpoint(%d) data(%d)\n", srcAddr, endpoint, value)

	if endpoint == 0x01 {
		if value == 0x01 {
			z.acc.Chan01.On.UpdateValue(characteristic.SwitchStateOn)
		} else {
			z.acc.Chan01.On.UpdateValue(characteristic.SwitchStateOff)
		}
	} else if endpoint == 0x02 {
		if value == 0x01 {
			z.acc.Chan02.On.UpdateValue(characteristic.SwitchStateOn)
		} else {
			z.acc.Chan02.On.UpdateValue(characteristic.SwitchStateOff)
		}
	} else if endpoint == 0x03 {
		if value == 0x01 {
			z.acc.Chan03.On.UpdateValue(characteristic.SwitchStateOn)
		} else {
			z.acc.Chan03.On.UpdateValue(characteristic.SwitchStateOff)
		}
	}
	return nil
}

func (z *zigbeeSwitch) onValueUpdate(value uint8, channel uint8) {
	data := make([]byte, 6)
	data[0] = 0x02
	netAddr, _ := hex.DecodeString(z.NetAddr)
	copy(data[1:], netAddr)
	data[3] = 0x01
	data[4] = channel
	data[5] = value
	msg := &Message{MsgT: SerialLinkMsgTypeOnOffNoEffects, Data: data}
	sdata, err := msg.Encode()
	if err != nil {
		log.Fatal(err)
	}
	client.task <- sdata
}

func newzigbeeSwitch(zigbee *ZigbeeDescriptor) *zigbeeSwitch {
	sw := &zigbeeSwitch{
		ZigbeeDescriptor: zigbee,
		acc:              accessory.NewSwitchChan3(zigbee.MacAddr),
	}

	sw.acc.Chan01.On.OnValueUpdateFromConn(func(conn *websocket.Conn, c *characteristic.Characteristic, newValue, oldValue interface{}) {
		if newValue == characteristic.SwitchStateOn {
			sw.onValueUpdate(characteristic.SwitchStateOn, 1)
		} else {
			sw.onValueUpdate(characteristic.SwitchStateOff, 1)
		}
	})

	sw.acc.Chan02.On.OnValueUpdateFromConn(func(conn *websocket.Conn, c *characteristic.Characteristic, newValue, oldValue interface{}) {
		if newValue == characteristic.SwitchStateOn {
			sw.onValueUpdate(characteristic.SwitchStateOn, 2)
		} else {
			sw.onValueUpdate(characteristic.SwitchStateOff, 2)
		}
	})

	sw.acc.Chan03.On.OnValueUpdateFromConn(func(conn *websocket.Conn, c *characteristic.Characteristic, newValue, oldValue interface{}) {
		if newValue == characteristic.SwitchStateOn {
			sw.onValueUpdate(characteristic.SwitchStateOn, 3)
		} else {
			sw.onValueUpdate(characteristic.SwitchStateOff, 3)
		}
	})

	return sw
}
